package engine;

/**
 * Created by andreyparamonov on 25.04.16.
 */
public class MyLogger {

    public static void l(String msg){
        System.out.print(msg);
    }

    public static void ln(String msg){
        System.out.println(msg);
    }

    public static void e(String msg){
        System.out.println("ERROR: "+msg);
    }

    public static void s(String msg){
        System.out.println("++++++ "+msg + " ++++++");
    }
}
