package engine;



import engine.machines.BasicTouringMachine;

import java.io.IOException;

/**
 * Created by andreyparamonov on 25.04.16.
 */
public class RunExample {


    static int m1 = -1, m2 = -1;
    //  an example how to use it

    public static void main(String[] args){


        /**
         * You can change the run parameters in the bulk constructor
         * */


        /**
         * Console commands:
         *  anlways end the command with a ";"
         *
         *  to multiply  3 and 4, type: 3*4;
         *  to add  3 and 4, type: 3+4;
         *  to interrupt loop : exit;
         *  @return opearator as char
         * */

        while ( true) {
            // read console input, see above
            char operator = readInput();

            // execute if success read and both numbers are >= 0
            if (m1 >= 0 && m2 >= 0) {
                Result result = null;

                if( operator == '*') {
                    result = BasicTouringMachine.multiply(m1,m2);
                }
                else if(operator == '+'){
                    result = BasicTouringMachine.add(m1,m2);
                }
                else {
                    MyLogger.e("Error operator:" + operator);
                }

                if( result != null) {
                    MyLogger.ln("DONE in " + result.stepsCount + " steps. Result =  " + result.result );
                }
            }

            //reset
            m1 =-1;
            m2 =-1;
        }
    }

    /**
     * @return opearator as char*/

    private static char readInput() {
        // read input command
        String in ="";
        char operator = ',';
        try {
            int read = 0;

            while (read != ';') {
                if(System.in.available() >0) {
                    read = (System.in.read());
                    char input = (char) (read);
                    in += input;

                }
            }

        } catch (IOException e) {
            MyLogger.e("Error console");
        }

        // Parse command
        if((in.length() >0)) {
            if(in.contains("*"))operator = '*';
            else if(in.contains("+"))operator = '+';

            in = in.trim().replaceAll(";","").trim();
            String splitOp = operator== '*' ? "\\*" : "\\+";
            try {
                m1 = Integer.parseInt(in.split(splitOp)[0]);
                m2 = Integer.parseInt(in.split(splitOp)[1]);

            } catch (Exception e) {
                if(in.contains("exit"))System.exit(0);
                MyLogger.e("Could not parse input: " + in + "/" + operator);
            }


        }

        return operator;
    }
}
