package engine;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyparamonov on 25.04.16.
 */
public class Operation {


    static int countNumbers =0;
    int pointNumber;



    final List<Arrow> mOperators = new ArrayList<>();

    /**
     * Constructors and bulk constructors
     * */
    public Operation(){
        pointNumber = countNumbers++;
    }


    public Operation linkSelf(Arrow operator){
        return link(operator,this);
    }

    public Operation link(Arrow operator, Operation point){
        operator.setNext(point);
        mOperators.add(operator);
        return this;
    }

    /**
     * @return null if finished, or the next point to execute*/

    public Operation executeAndGetNext(Stripe[] stripe){

        Operation next = null;

        if(mOperators.isEmpty())return null;



        for (Arrow o : mOperators) {
            boolean is = true;

            for (int index =0;index< stripe.length; index++) {
                is &= stripe[index].read() == o.read(index);
            }

            if( is){
                for (int index =0;index< stripe.length; index++) {
                    char input = stripe[index].read();

                    if (o.read(index) == input) {
                        next = execute(index, stripe, o);
                    }
                }

                return next;
            }


        }

        return next;
    }

    private Operation execute(int index, Stripe[] stripe, Arrow o) {
        stripe[index].write(o.write(index));

        stripe[index].move(o.move(index));

        return o.nextPoint;
    }


    public static Operation start(){
        countNumbers=0;
        return new Operation();
    }

    public int getNumber() {
        return pointNumber;
    }

    public int getLines() {
        return mOperators.get(0).size();
    }
}
