package engine;


/**
 * Created by andreyparamonov on 26.04.16.
 */
public class Stripe {

    public static final char BREAK = '_';
    public static final char ZERO = '0';
    public static final char ONE = '1';

    public static final char RIGHT = 'R';
    public static final char LEFT = 'L';
    public static final char NONE = 'N';


    private int currentPosition;
    char[] stripe;
    int countLeft,countRight;

    public Stripe(int countLeft, int countRight){
        stripe = new char[countLeft + 1 + countRight];
        currentPosition = countLeft;
        this.countLeft = countLeft;
        this.countRight = countRight;
        clear();
    }


    public void set(int multiplier1, int multiplier2){

        int i =0;
        for(; i < multiplier1 && i < countRight; i ++){
            stripe[i + currentPosition] = ZERO;
        }
        stripe[i++ + currentPosition] = ONE;
        for(; i < multiplier1+multiplier2+1 && i < countRight; i ++){
            stripe[i + currentPosition] = ZERO;
        }

    }

    public boolean move(char move){
        switch (move){
            case RIGHT:
                if(currentPosition  < stripe.length){
                    currentPosition++;
                }
                else return false;
                break;

            case LEFT:
                if(currentPosition  > 0){
                    currentPosition--;
                }
                else return false;
                break;
            case NONE:


                break;

        }
        return true;
    }

    public char read(){
        try {
            return stripe[currentPosition];
        }
        catch (IndexOutOfBoundsException ex){
            MyLogger.e("Output too big to process, try to change the size of the Stripe");
            return stripe[stripe.length-1];
        }
    }

    public Stripe write(char a){
        try {
            stripe[currentPosition] =a;
        }
        catch (IndexOutOfBoundsException ex){
            MyLogger.e("Output too big to process, try to change the size of the Stripe");
        }

        return this;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    @Override
    public String toString() {
        String s1 =  getOffset(new String(stripe));

        try {
            s1=  s1.substring(0,countLeft) + "|" + s1.charAt(countLeft)+ "|" + s1.substring(countLeft+1,s1.length());
        }
        catch (IndexOutOfBoundsException ex){
            MyLogger.e("Output too big to process, try to change the size of the Stripe");
        }


        return s1;
    }

    public String getOffset(String input) {

        int offset = (countLeft -currentPosition);


        if(offset <0){
            offset *=-1;
            for(int i =0; i < offset; i ++)input +=":";
            input = input.substring(offset,input.length()) ;
        }

        return input;
    }

    public int countZeros() {
        int count  =0;
        for(char a : stripe){
            count = a== ZERO ? count+1:count;
        }


        return count;
    }

    public void clear() {
        for(int i =0; i < stripe.length; i ++){
            stripe[i] = BREAK;
        }
    }
}
