package engine;

import engine.machines.BasicTouringMachine;

/**
 * Created by andreyparamonov on 26.04.16.
 */
public class Result {

    BasicTouringMachine machine;
    public int result =0;
    public int stepsCount=0;

    public Result(BasicTouringMachine machine,int res, int count){
        this.machine = machine;
        result = res;
        stepsCount = count;
    }
}
