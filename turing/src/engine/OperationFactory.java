package engine;


/**
 * Created by andreyparamonov on 25.04.16.
 */
public class OperationFactory {


    public static Operation ADDER(){

        Operation start = Operation.start();
        Operation p1 = new Operation();
        Operation p2 = new Operation();
        Operation p3 = new Operation();


        start.link(Arrow.parse("0/_,R"),p1);
        start.link(Arrow.parse("1/_,R"),p2);

        p1.linkSelf(Arrow.parse("0/0,R"));
        p1.link(Arrow.parse("1/0,R"),p2);

        p2.linkSelf(Arrow.parse("0/0,R"));
        p2.link(Arrow.parse("1/_,R"),p3);

        return start;
    }


    public static Operation MULTIPLIER(){

        Operation start = Operation.start();
        Operation p1 = new Operation();
        Operation p2 = new Operation();
        Operation p3 = new Operation();
        Operation end = new Operation();


        start.linkSelf(Arrow.parse("0__/_0_,RRN"));
        start.link(Arrow.parse("1__/___,RNN"),p1);

        p1.link(Arrow.parse("0__/0__,NLN"),p2);
        p1.link(Arrow.parse("___/___,NNN"),end);

        p2.linkSelf(Arrow.parse("00_/00_,NLN"));
        p2.link(Arrow.parse("0__/0__,NRN"),p3);


        p3.linkSelf(Arrow.parse("00_/000,NRR"));
        p3.link(Arrow.parse("0__/___,RNN"),p1);


        return start;
    }

}
