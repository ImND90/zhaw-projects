package engine;



/**
 * Created by andreyparamonov on 26.04.16.
 */
public  class Arrow {

    /**
     *
     * -------- 1/0,R ------>
     *
     * */

    private final char[] read;
    private final char[] write;
    private final char[] move;
    public Operation nextPoint = null;


    public Arrow(char r[], char w[], char m[]){
        read = r;
        move = m;
        write = w;
    }

    public static Arrow parse(String input) {

        try {
            String r, w;
            String m;

            String[] rw = input.split(",");

            r = rw[0].trim().split("/")[0];
            w = rw[0].trim().split("/")[1];
            m = rw[1];

            char[] read = new char[r.length()];
            char[] write = new char[w.length()];
            char[] move = new char[m.length()];

            for(int i =0 ; i < r.length(); i++){
                read[i] = r.charAt(i);
            }

            for(int i =0 ; i < w.length(); i++){
                write[i] = w.charAt(i);
            }

            for(int i =0 ; i < m.length(); i++){
                move[i] = m.charAt(i) ;
            }

            return new Arrow(read,write,move);
        }
        catch (Exception e){
            MyLogger.e("Could not parse this operator : "  + input);
            return null;
        }

    }

    public char read(int index){
        return read[index];
    }
    public char write(int index){
        return write[index];
    }
    public char move(int index){
        return move[index];
    }

    public Arrow setNext(Operation point){
        nextPoint = point;
        return this;
    }


    public String print() {
        return ""; //TODO
       // return (String.format(FORMAT_OPERATION,read,write,move.name()));
    }

    public int size() {
        return read.length;
    }
}
