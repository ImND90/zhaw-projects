package engine.machines;


import engine.*;

/**
 * Created by andreyparamonov on 26.04.16.
 */
public class BasicTouringMachine {

    public static final int LEFT_RIGHT_OFFSET = 15;

    // options
    protected int maxSteps = 65001;
    protected boolean printWholeStripe = true;
    protected boolean printstatusAndSteps = true;

    protected final int intdexResult;
    // variables
    protected Stripe stripe[];

    protected int currentOperation =0;
    protected int steps =0;

    protected final Operation entryPoint;

    protected BasicTouringMachine(int stripeOffset, int indexLineResult,  Operation entryPoint){

        this.intdexResult = indexLineResult;
        this.entryPoint = entryPoint;
        stripe = new Stripe[entryPoint.getLines()];
        for( int i =0; i < stripe.length ; i++){
            stripe[i] = new Stripe(stripeOffset,stripeOffset);
        }

    }

    public BasicTouringMachine limitSteps(int steps){
        maxSteps = steps;
        return this;
    }
    public BasicTouringMachine printWholeStripe(boolean print){
        printWholeStripe = print;
        return this;
    }
    public BasicTouringMachine printStatusAndSteps(boolean print){
        printstatusAndSteps = print;
        return this;
    }



    public static Result multiply(int... args){
        int result = args[0] * args[1];
        BasicTouringMachine m = new BasicTouringMachine(Math.max(LEFT_RIGHT_OFFSET,result),2, OperationFactory.MULTIPLIER());
        return m.run(args);
    }
    public static Result add(int... args){
        int result = args[0] + args[1];
        BasicTouringMachine m = new BasicTouringMachine(Math.max(LEFT_RIGHT_OFFSET,result),0, OperationFactory.ADDER());
        return m.run(args);
    }



    @Override
    public String toString() {
        String s ="";

        if(printstatusAndSteps) {
            s += "//////////////////\n/// [P" +
                    currentOperation + "] - Steps:[" +
                    steps + "]\n";

        }
        for (Stripe b: stripe){
            s+= b.toString() + "\n";
        }

        return s;
    }

    /**
     * @return null if finished of error occured,
     * else the next operation to execute
     * */
    protected Operation executeNext(Operation op){

        currentOperation = op.getNumber();
        op = op.executeAndGetNext(stripe);
        steps++;

        if(printWholeStripe) {
            MyLogger.ln(toString());
        }

        if(steps > maxSteps){
            MyLogger.e( "Aborted due to MAX steps = " + maxSteps);
            return null;
        }

        return op;
    }


    private Result run(int... args){


        stripe[0].set(args[0],args[1]);
        for(int i = 1;i < stripe.length; i++){
            stripe[i].clear();
        }


        Operation op = entryPoint;

        // Print first status
        MyLogger.ln(toString());

        while (op != null && steps < maxSteps){
            op = executeNext(op);
            steps++;
        }


        return new Result(this,stripe[intdexResult].countZeros(),steps);
    }

    public Stripe[] getStripes() {
        return stripe;
    }

    public int getCurrentOperation() {
        return currentOperation;
    }

    public int getStepsNeeded() {
        return steps;
    }
}
